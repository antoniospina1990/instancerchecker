﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Logger;
using System.Configuration;
//using System.Configuration;

namespace IstanceGuardianService
{
    
    class CheckInstanceControl

    {

        private string WATCHDOG_APP_NAME = "InstancerChecker";
        private static string MONITORED_APP_NAME = ConfigurationManager.AppSettings["MONITORED_APP_NAME"].ToString();
        private static string applicationDirectory = ConfigurationManager.AppSettings["APPLICATION_DIRECTORY"].ToString();
        private static string BATCH_TO_RUN = ConfigurationManager.AppSettings["BATCH_TO_RUN"];
        private static string FILE_PATH_TO_BE_VALUETED = ConfigurationManager.AppSettings["FILE_PATH_TO_BE_VALUETED"];
        private static string FILE_PATTERN_TO_BE_VALUETED = ConfigurationManager.AppSettings["FILE_PATTERN_TO_BE_VALUETED"];
        private static Int16 LAST_NUMBER_LINES_TO_VALUATE = Convert.ToInt16(ConfigurationManager.AppSettings["LAST_NUMBER_LINES_TO_VALUATE"]);
        private static int DELAYED_TIME_AFTER_APPLICATION_RESTART = Convert.ToInt32(ConfigurationManager.AppSettings["DELAYED_TIME_AFTER_APPLICATION_RESTART"]);
        private static int MONITORING_INTERVAL = Convert.ToInt32(ConfigurationManager.AppSettings["MONITORING_INTERVAL"]);
        private static LogWriter log = LogWriter.Instance;

        /// <summary>
        /// 
        /// </summary>
        //
        public void Start()
        {
            log.WriteToLog("Process to be checked : " + MONITORED_APP_NAME);
            log.WriteToLog("Executeable file path : " + applicationDirectory + "\\"+MONITORED_APP_NAME + ".exe");
            CheckWhatchDogIstance();
        }
        private void CheckWhatchDogIstance()
        {
            /// Check if another instance of this application is running  
            /// If True self-terminate  
            /// Else continue  
            /// 
            log.WriteToLog("Watch dog checker...");
            try
            {
                Process[] pname = Process.GetProcessesByName(WATCHDOG_APP_NAME);
                if (pname.Length > 1)
                {
                    log.WriteToLog("services already running. I kill the old one and i run a new one");
                    Process.GetCurrentProcess().Kill();
                }
            }
            catch (Exception ex)
            {
                log.WriteToLog("CheckDuplicateInstancerChecker Exception: " + ex.StackTrace);
            }
            //try
            //{
            //    monitoringInterval = Convert.ToInt32(ConfigurationManager.AppSettings["MonitoringInterval"]);
            //}
            //catch (Exception ex)
            //{
            //    monitoringInterval = 5000;
            //    Debug.WriteLine("ApplicationWatcher Exception2: " + ex.StackTrace);
            //}
            try
            {
                Thread watchDogThread = new Thread(new ThreadStart(StartAppMonitoring));
                log.WriteToLog("Watch dog thread starts");
                watchDogThread.Start();
            }
            catch (Exception ex)
            {
                log.WriteToLog(ex.StackTrace);
            }
        }

        private static bool MonitoredAppExists()
        {
            try
            {
                
                Process[] processList = Process.GetProcessesByName(MONITORED_APP_NAME);
                if (processList.Length == 0)
                {
                    log.WriteToLog("Proces not found ");
                    return false;
                }
                else
                {
                    log.WriteToLog("Proces found ");
                    return true;
                }
            }
            catch (Exception ex)
            {
                log.WriteToLog("MonitoredAppExists Exception: "+ ex.StackTrace);
                return true;
            }
        }
        private static void StartAppMonitoring()
        {
            int monitoringInterval = MONITORING_INTERVAL;
            while (true)
            {
                
                /// A lock file is used to temporarily disable the watchdog from
                /// monitoring and reviving the monitored application for debugging purposes.  
                if (!File.Exists("watchdoglock"))
                {
                    monitoringInterval = MONITORING_INTERVAL;
                    if (!MonitoredAppExists())
                    {
                        log.WriteToLog("Application not running...");
                        if (File.Exists(applicationDirectory + "\\" + MONITORED_APP_NAME + ".exe")&& File.Exists(applicationDirectory + "\\" + BATCH_TO_RUN))
                        {
                            log.WriteToLog("Trying to run the Batch file...");
                            //string strAppPath;
                            //strAppPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ProgramFiles) + @"\FolderName\AppName.exe";
                            System.Diagnostics.Process.Start(applicationDirectory + "\\" + BATCH_TO_RUN);
                            //Process.Start(applicationDirectory + "\\" + BATCH_TO_RUN );
                        }
                        else
                        {
                            log.WriteToLog("ApplicationWatcher StartAppMonitoringMonitored Application or batch file not found at: " + applicationDirectory + "\\" + MONITORED_APP_NAME + ".exe || "+ BATCH_TO_RUN);  
                        }
                    }
                    else
                    {
                        string YYYY, DD, MM;
                        YYYY = DateTime.Today.ToString("yyyy");
                        DD = DateTime.Today.ToString("dd");
                        MM = DateTime.Today.ToString("MM");
                       string completeFilePath = String.Format("{0}\\{1}-{2}-{3}.txt", FILE_PATH_TO_BE_VALUETED, DD, MM, YYYY);
                        try {
                            //string fileToCheck = ;
                            if (File.Exists(completeFilePath))
                            {
                                //File file = File.OpenRead(String.Format("{0}\\{1}-{2}-{3}.txt", FILE_PATH_TO_BE_VALUETED, DD, MM, YYYY));
                                using (StreamReader reader = new StreamReader(completeFilePath))
                                //using (StreamReader reader = new StreamReader(args[0].ToString()))
                                {
                                    string[] stringSeparators = new string[] { "\r\n" };
                                    List<string> text = Convert.ToString(reader.ReadToEnd()).Split(stringSeparators, StringSplitOptions.None).Reverse().Take(LAST_NUMBER_LINES_TO_VALUATE).Where(x => x.Contains("_ERROR") || x.Contains("StartUp")).ToList();
                                    //List<string> text = Convert.ToString(reader.ReadToEnd()).Split(stringSeparators, StringSplitOptions.None).Reverse().Take(LAST_NUMBER_LINES_TO_VALUATE).ToList();
                                    if (text.Count > 1)
                                    {
                                        if (!text[0].Contains("StartUP") && text[1].Contains("_ERROR"))
                                        {
                                            log.WriteToLog("i found " + text.Count.ToString() + " Errors and no Application restarts");
                                            Process processList = Process.GetProcessesByName(MONITORED_APP_NAME).First();
                                            processList.Kill();
                                            Process.Start(applicationDirectory + "\\" + BATCH_TO_RUN);
                                            log.WriteToLog("I have restarted the batch file " + BATCH_TO_RUN + " and i delayed Instacer checker to " + DELAYED_TIME_AFTER_APPLICATION_RESTART + " seconds");
                                            monitoringInterval = DELAYED_TIME_AFTER_APPLICATION_RESTART;
                                        }
                                    }
                                }
                                //List<string> text = File.ReadLines(String.Format("{0}\\{1}-{2}-{3}.txt", FILE_PATH_TO_BE_VALUETED, DD, MM, YYYY)).Reverse().Take(LAST_NUMBER_LINES_TO_VALUATE).Where(x => x.Contains("_ERROR") || x.Contains("StartUp")).ToList();
                                //if (!text[0].Contains("StartUP") && text[1].Contains("_ERROR"))
                                //{
                                //    log.WriteToLog("i found " + text.Count.ToString() + " Errors and no Application restarts");
                                //    Process processList = Process.GetProcessesByName(MONITORED_APP_NAME).First();
                                //    processList.Kill();
                                //    Process.Start(applicationDirectory + "\\" + MONITORED_APP_NAME + ".exe");
                                //    log.WriteToLog("I have restarted the application " + MONITORED_APP_NAME + " and i delayed Instacer checker to " + DELAYED_TIME_AFTER_APPLICATION_RESTART + " seconds");
                                //    monitoringInterval = DELAYED_TIME_AFTER_APPLICATION_RESTART;
                                //}
                            }
                        }
                        catch (Exception ex)
                        {
                            log.WriteToLog("Error durin file valuation : " + ex.StackTrace + Environment.NewLine + ex.InnerException);
                        }
                    }
                }
                else
                {
                    log.WriteToLog("ApplicationWatcher StartAppMonitoringwatchdoglock found.");  
                }
                Thread.Sleep(monitoringInterval);
            }
        }
    }
}

